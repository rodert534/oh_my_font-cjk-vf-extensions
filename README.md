# Oh My Font CJK Extension for CJK Languages
Add variable font for CJK.

### More information about OMF
- [GitLab](https://gitlab.com/nongthaihoang/oh_my_font)
- [Wiki for OMF](https://gitlab.com/nongthaihoang/oh_my_font/-/wikis/home)
- [Telegram Channel](https://t.me/ohmyfont_channel)
- [Telegram Discussion](https://t.me/ohmyfont)

### How to istall
- Once installed the main module should generate a folder called `OhMyFont` at internal storage.

- Download
  - [NotoSansCJK-VF-Android.otf.ttc](https://github.com/WordlessEcho/Noto-CJK-VF-Magisk/blob/main/system/fonts/NotoSansCJK-VF-Android.otf.ttc?raw=true)
  - [NotoSerifCJK-VF-Android.otf.ttc](https://github.com/WordlessEcho/Noto-CJK-VF-Magisk/blob/main/system/fonts/NotoSerifCJK-VF-Android.otf.ttc?raw=true)
- Rename `NotoSansCJK-VF-Android.otf.ttc` to `NotoSansCJK-VF.otf.ttc`
- Rename `NotoSerifCJK-VF-Android.otf.ttc` to `NotoSerifCJK-VF.otf.ttc`
- Put them into OhMyFont folder.
- Put [02_NotoCJKVF.sh](https://gitlab.com/rodert534/oh_my_font-cjk-vf-extensions/-/raw/master/02_NotoCJKVF.sh?inline=false)
- Flash again the main module.
- Done.

### Sepcial Thanks
- [Wordless Echo](https://github.com/WordlessEcho)
- _@_ [Nông Thái Hoàng](https://t.me/nongthaihoang)

### Link
- [The original Magisk module](https://github.com/WordlessEcho/Noto-CJK-VF-Magisk)
